# Text-Based Representations with Interpretable Machine Learning Reveal Structure-Property Relationships for Polybenzenoid Hydrocarbons

## Content
* Source code for calculations
* Source code for obtaining the LALAS and augmented-LALAS representations
* Source code for obtaining the LALAS/augmented-LALAS feature vectors (LFVs)
* Data used as input (COMPAS-1D dataset and generated LFVs)

## How to cite this work
If you use any part of this work, please cite the following:
S. Fite, A. Wahab, E. Paenurk, Z. Gross, and R. Gershoni-Poranne, Text-based representations with interpretable machine learning reveal structure–property relationships of polybenzenoid hydrocarbons, DOI: 10.1002/poc.4458

## Support
For support or to report any issues, please contact: porannegroup /at/ technion.ac.il

## Authors and acknowledgment
This work was conducted under the supervision of Prof. Dr. Renana Gershoni-Poranne (Technion/ETH Zurich). The following people contributed to this project: 
1. Shachar Fite (Technion)
2. Alexandra Wahab (ETH Zurich)
3. Dr. Eno Paenurk (ETH Zurich)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Prof. Zeev Gross, Dr. Alexandra Tsybizova, Felix Fleckenstein. 

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The code and data in this repository are provided free-of-charge. They are licensed under a CC-BY-NC-SA license. Please cite the relevant literature if you use them.
